function compute_global_blendshapes()
    close all
    
    % Path where you uncompress all .obj
    blendshape_path = '.\blendshapes\tri'; % ��Actualiza el path!!
    
    % Read OBJ files
    [V00,F] = read_vertices_and_faces_from_obj_file(strcat(blendshape_path, '/neutral-tri.obj'));
    % V00 es una matrix vertices de tama�o [numero_vertices x 3]
    num_vertices = size(V00,1); % Lee n�mero de vertices
    % Vectoriza malla 00
    V00 = vertcat(V00(:,1),V00(:,2),V00(:,3));
    
    % ����Leer m�s OBJ aqu�!!!!
    % Read OBJ files
    [V01,~] = read_vertices_and_faces_from_obj_file(strcat(blendshape_path, '/mouth-open-tri.obj'));
    % Vectoriza malla 00
    V01 = vertcat(V01(:,1),V01(:,2),V01(:,3));
    
    % Read OBJ files
    [V02,~] = read_vertices_and_faces_from_obj_file(strcat(blendshape_path, '/right-eye-down-tri.obj'));
    % Vectoriza malla 00
    V02 = vertcat(V02(:,1),V02(:,2),V02(:,3));
    
    figure;

    %  Creamos un vector t con parametros alpha que utilizaremos
    t=0:0.2:1;
    t = [t flip(t)];
    iterations = 0;
    
    % Loop infinito para ver los resultados
    while 1
       
        % Iteraci�n actual. Utilizamos m�dulo para siempre tener un n�mero
        % de iteraci�n > tama�o del vector de alphas t.
        iterations = mod(iterations,size(t,2)) + 1;

        % Alpha correspondiente a la iteraci�n actual
        alpha = t(iterations);

        % Stores current viewpoint
        [az, el] = view; 

        % ����Calcular blendshape!!!!
        V_blend = V00;
        V_blend = V_blend + (1.1*(1-alpha) * (V01(:) - V00(:)));
        V_blend = V_blend + (1.1*(1-alpha) * (V02(:) - V00(:)));
        
        % Reshapes from vectorized mesh to [num_vertices,3]
        V_blend = reshape(V_blend,[num_vertices,3]);
        
        % Draws mesh
        trisurf(F,V_blend(:, 1),...
                  V_blend(:, 2),...
                  V_blend(:, 3),...
                  'FaceColor',[0.26,0.63,1.0 ],'EdgeColor','none','LineStyle','none','SpecularStrength',0.4);
      
        % Sets current viewpoint
        view (az, el);
       
        % Set up lighing
        light('Position',[-1.0,-1.0,100.0],'Style','infinite');
        lighting phong;
        
        % Set up axis
        axis equal
        axis([-1.5 1.5 0 2 -1 1.5])
       
        drawnow;
    end
end